/******************************************************************************
   CÓDIGO DE TEST PARA COMANDOS Y CONFIGURACIONES DISPOSITIVO 1 Y 2
 ******************************************************************************/

#include <Arduino.h>
#include <rhio-pinmap.h>

/******************************************************************************
   INCLUDES
 ******************************************************************************/

#include "arduino_secrets.h"
#include <LiveObjects.h>

/******************************************************************************
   USER VARIABLES
 ******************************************************************************/

uint32_t messageRate =
    60000;             // stores the current data message rate in Milliseconds
unsigned long uptime;  // stores the device uptime (sent as fake sensor data)
unsigned long lastMessageTime =
    0;  // stores the time when last data message was sent

/******************************************************************************
   USER PROGRAM
 ******************************************************************************/

/*  Se crea un procedimiento por cada comando establecido para el dispositivo 1
y 2. Como no se tiene conectado ningún periférico como relés o sensores, se hace
parpadear el LED del ESP32 para comprobar que responde correctamente.

*/
/* PROCEDIMIENTO DE EJEMPLO


void blinkLED(const String arguments, String &response) {
  // example of 'arguments' content: "{\"time ON\":250,\"time
OFF\":750,\"repetitions\":5}" StaticJsonDocument<128> incomingArguments;  //
creation of a JSON document called "incomingArguments" that will holds the
arguments deserializeJson(incomingArguments, arguments);  // extraction of JSON
data

  int timeOn = incomingArguments["time ON"];  // arguments are now accessible
using their name int timeOff = incomingArguments["time OFF"]; int reps =
incomingArguments["repetitions"]; unsigned long elaspedTime = millis();   //
will keep track of time in order to compute the animation duration

  pinMode(LED, OUTPUT);
  for (byte i = 0; i < reps; i++) {
    digitalWrite(LED, HIGH);
    delay(timeOn);
    digitalWrite(LED, LOW);
    delay(timeOff);
  }

  elaspedTime = millis() - elaspedTime;

  StaticJsonDocument<128> outgoingResponse;  // creation of a JSON document that
will hold the response outgoingResponse["animation duration (milliseconds)"] =
elaspedTime;  // adding reponse item (you can add several by repeating the
line): serializeJson(outgoingResponse, response);  // exporting JSON in
'reponse' String
  // example of 'response' content: "{\"animation duration
(milliseconds)\":5000}"
}

*/

void comandoLock(const String arguments, String &response) {
  // Si recibe LOCK, se enciende el LED.
  // Si recibe UNLOCK, se apaga el LED.

  int lock = 0;

  StaticJsonDocument<128>
      incomingArguments;  // creation of a JSON document called
                          // "incomingArguments" that will holds the arguments
  deserializeJson(incomingArguments, arguments);  // extraction of JSON data

  lock = incomingArguments["LOCK"];  // arguments are now accessible using their
                                     // name

  if (lock == 1) {
    digitalWrite(LED, HIGH);
  }
  /*
    elaspedTime = millis() - elaspedTime;

    StaticJsonDocument<128> outgoingResponse;  // creation of a JSON document
    that will hold the response outgoingResponse["animation duration
    (milliseconds)"] = elaspedTime;  // adding reponse item (you can add several
    by repeating the line): serializeJson(outgoingResponse, response);  //
    exporting JSON in 'reponse' String
    // example of 'response' content: "{\"animation duration
    (milliseconds)\":5000}"
  */
}

void comandoUnlock(const String arguments, String &response) {
  // Si recibe LOCK, se enciende el LED.
  // Si recibe UNLOCK, se apaga el LED.

  int unlock = 0;

  StaticJsonDocument<128>
      incomingArguments;  // creation of a JSON document called
                          // "incomingArguments" that will holds the arguments
  deserializeJson(incomingArguments, arguments);  // extraction of JSON data

  unlock = incomingArguments["UNLOCK"];  // arguments are now accessible using
                                         // their name

  if (unlock == 1) {
    digitalWrite(LED, LOW);
  }
  /*
    elaspedTime = millis() - elaspedTime;

    StaticJsonDocument<128> outgoingResponse;  // creation of a JSON document
    that will hold the response outgoingResponse["animation duration
    (milliseconds)"] = elaspedTime;  // adding reponse item (you can add several
    by repeating the line): serializeJson(outgoingResponse, response);  //
    exporting JSON in 'reponse' String
    // example of 'response' content: "{\"animation duration
    (milliseconds)\":5000}"
  */
}

void comandoUnlock(const String arguments, String &response) {
  // Si recibe LOCK, se enciende el LED.
  // Si recibe UNLOCK, se apaga el LED.

  int unlock = 0;

  StaticJsonDocument<128>
      incomingArguments;  // creation of a JSON document called
                          // "incomingArguments" that will holds the arguments
  deserializeJson(incomingArguments, arguments);  // extraction of JSON data

  unlock = incomingArguments["UNLOCK"];  // arguments are now accessible using
                                         // their name

  if (unlock == 1) {
    digitalWrite(LED, LOW);
  }
  /*
    elaspedTime = millis() - elaspedTime;

    StaticJsonDocument<128> outgoingResponse;  // creation of a JSON document
    that will hold the response outgoingResponse["animation duration
    (milliseconds)"] = elaspedTime;  // adding reponse item (you can add several
    by repeating the line): serializeJson(outgoingResponse, response);  //
    exporting JSON in 'reponse' String
    // example of 'response' content: "{\"animation duration
    (milliseconds)\":5000}"
  */
}

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
  Serial.print("\n*** Live Objects on Arduino MKR boards, revision ");
  Serial.print(SW_REVISION);
  Serial.println("***");

  // Declaring a simple commands hadled by the function 'blinkLED'.
  lo.addCommand("comandoLock", comandoLock);
  lo.addCommand("comandoUnlock", comandoUnlock);
  lo.setSecurity(TLS);
  lo.begin(MQTT, TEXT, true);
  lo.connect();  // connects to the network + Live Objects
}

void loop() {
  if (millis() - lastMessageTime > messageRate) {
    // collect data periodically
    Serial.println("Sampling data");
    uptime = millis();
    lo.addToPayload("uptime",
                    uptime);  // adding 'uptime' value to the current payload

    Serial.println("Sending data to Live Objects");
    lo.sendData();  // send the data to Live Objects
    lastMessageTime = millis();
  }

  lo.loop();  // don't forget to keep this in your main loop
}
